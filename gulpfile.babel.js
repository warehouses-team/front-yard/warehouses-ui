'use strict';

import path from 'path';
import del from 'del';
import gulp from 'gulp';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'gulp-autoprefixer';

const config = {
    "styles": {
        "src": "scss/styles.scss",
        "dest": "css",
        "watch": "scss/**/*.scss",
        "sass": {
            "outputStyle": "expanded"
        },
        "autoprefixer": {
            "browsers": "last 2 versions",
            "cascade": false
        }
    }
}

const styles = config.styles;

const onChange = (file) => {
    console.log(`File modified: "${path.basename(file.path)}"`);
};

gulp.task('build:styles', () => {
    gulp.src(styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass(styles.sass).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write(''))
    .pipe(gulp.dest(styles.dest));
});

gulp.task('clean', () => { del.sync(['./css']) });
gulp.task('build', ['build:styles']);
gulp.task('clean:build', ['clean', 'build']);

gulp.task('watch',() => {
    gulp.watch(styles.watch, ['build:styles']).on('change', onChange);
});

gulp.task('default', ['clean:build', 'watch']);